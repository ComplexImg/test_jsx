'use strict';
import Button from './components/Button';
import React from 'react';
import ReactDOM from 'react-dom';
ReactDOM.render(
    <div>
        <h2>Buttons</h2>
        <div>Button with onClick: <Button onClick={() => alert('ouch')}>Click me</Button></div>
        
        <div>A link: <Button href="http://reactjs.com">Follow me</Button></div>
        
        <div>Custom class name: <Button className="custom">I do nothing</Button></div>
    </div>,
    document.getElementById('pad')
);