import classNames from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';


// Когда компонент настолько прост (и такое бывает!) и не нуж-
// дается в поддержке состояния, для его определения можно
// воспользоваться функцией. Тело этой функции станет заме-
// ной вашего метода render()
function Button(props) {
    let cssclasses = classNames('Button', props.className);
    return props.href
        ? <a {...props} className={cssclasses} />
        : <button {...props} className={cssclasses} />;
}
Button.propTypes = {
    href: PropTypes.string,
};

export default Button